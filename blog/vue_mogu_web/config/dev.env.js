'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
 
  VUE_MOGU_WEB: '"https://blog.itspeeding.com"',
  PICTURE_API: '"https://gateway.itspeeding.com/mogu-picture"',
  WEB_API: '"http://127.0.0.1:8607/mogu-web"',
  SEARCH_API: '"https://gateway.itspeeding.com/mogu-search"',


})
